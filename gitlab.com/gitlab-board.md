# GitLab board documentation

**Table of contents:**

- [Location](#location)
- [Columns](#columns)
  - [Open](#open)
  - [Creative WIP](#creative-wip)
  - [Creative Done](#creative-done)
  - [Specs/Requirements WIP](#specsrequirements-wip)
  - [Specs/Requirements Done](#specsrequirements-done)
  - [Dev WIP](#dev-wip)
  - [Dev Done](#dev-done)
  - [Translations WIP](#translations-wip)
  - [Translations Done](#translations-done)
  - [QA WIP](#qa-wip)
  - [QA Done](#qa-done)
  - [Closed](#closed)
- [Labels](#labels)
  - [Column labels](#column-labels)  
  - [Resource labels](#resources-labels)
  - [Other labels](#other-labels)
- [Problems in the process](#problems-in-the-process)  
  - [Too many issues in a column](#too-many-issues-in-a-column)
  - [Too much time in a column](#too-much-time-in-a-column)

## Location

The GitLab board for adblockplus.org can be found [here](https://gitlab.com/eyeo/websites/web.adblockplus.org/boards/575718).

## Columns

In order to easily assess at which step of the workflow a project is, at any moment in time, we use an extended [Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) board. Each major step from the workflow has its own `WIP` and `Done` columns.

Issues are ordered in a descending order, based on their priority. Team members should always pick the issues at the top of any column to start work on.

**Important:** A large number of issues in a column can indicate either a bottleneck or a [problem with the process](#problems-in-the-process).

Here's a presentation of each column and what happens when an issue (ticket) exists there.

### Open

This is the unprioritized backlog and contains all issues that are still in the concept phase. This column contains ideas, draft projects or requests from various stakeholders.

Issues added here are, in general, not ready to be worked on. In order for an issue to be ready for entering development, a member of the team, usually the **Product Manager** or the **Technical Project Manager** reviews the issue. This happens in order to gather any additional information, identify potential risks and prioritize accordingly.

### To Do

Issues in this column are ready to be picked up by the team. The issues here include updated descriptions and detailed information so that members of the team can start work right away. 

### Creative WIP

This column contains all projects that are currently being worked on by **Designer** and/or **Copywriter**.
Any feedback rounds and necessary adjustments (including review with Legal or PM or any stakeholder) are handled in this column.

### Creative Done

All issues in this column have finalized and approved design(s) and copy.

This column also represents the backlog for the **Product Manager** or a member of the team who writes specifications.

### Specs/Requirements WIP

All issues in this column are currently having their specifications written down, so that a **Developer** and **QA Tester** can understand what to implement and how it's supposed to function.

As part of this step, reviews and any adjustments are made to the specifications document with the help of a Developer in most cases. Depending on the complexity, a **Technical Project Manager** and a **QA Analyst** might also review, ask questions and guide whoever is writing the specifications.

### Specs/Requirements Done

Issues in this column have the requirements written down either as part of the description (for small updates) or as specification documents (most projects).

This column also represents the backlog for a **Developer**. 

### Dev WIP

This column contains all issues that are currently being worked on by a **Developer**.

It is not considered _done_ and should not be moved in the [Dev Done](#dev-done) column until it passed code review and everything is prepared for a **Translation Manager** or **QA Analyst** to work on it.

### Dev Done

All issues here are ready to be picked up by a **Translation Manager** & **QA Analyst**. 

### Translations WIP

Issues that are in this column have been picked up by a **Translation Manager** and are currently being requested from the translation agency, then reviewed and updated. All of these steps take place in this column.

### Translations Done

This column contains all issues that have been translated and are ready to be picked up by a **QA Analyst**.

### QA WIP

In this column one can find all issues that are currently being worked on by a **QA Analyst**.

In this step, a **QA Analyst** identifies any issues and reports them back to the **Developer** or **Translation Manager**.
Based on the findings, an issue is moved from here to one of these:
 
 - [QA Done](#qa-done), if no bugs have been found;
 - [Dev WIP](#dev-wip), if a Developer is working on fixing a bug;
 - [Translations WIP](#translations-wip), if a Translations Manager needs to fix translations issues
 
**Exceptions:** Sometimes, small bug fixes either with development or translations are being handled while the issue is in the `QA WIP` column but anything that takes more time should be reflected in the board accordingly. Moving the issue in the appropriate column is left at the discretion of the team members working on fixing the bug(s).

### QA Done

All issues that are in this column have passed through all stages in the workflow and are ready to be pushed in production.

### Closed

This columns contains all issues that have been either dismissed (won't do) or finalized and pushed into production (live).

## Labels

Based on how they're used, we have:

### Column labels

These labels have been created in order to be able to generate columns in the GitLab board based on them. They also serve to give an overview of an issue's progress and identify at which step in the change process it's at. 

These should change as issues progress across the GitLab board. When updated properly, they serve as a guideline when an issue has passed to the next step. They also help identify bottlenecks. See the [problems in the process](#problems-in-the-process) section to learn how to spot them.

| Value | Description |
| -- | -- |
| `To Do` | Issue's ready to be worked on |
| `Creative WIP` | The design and copy are being worked on |
| `Creative Done` | Design and copy final and ready to be spec'ed |
| `Specs/Requirements WIP` | Specifications or requirements are being written |
| `Specs/Requirements Done` | Specifications written and reviewed, ready for development |
| `Dev WIP` | Currently in development or being prepared for QA & translations |
| `Dev Done` | Has been implemented and is ready for QA & translations |
| `Translations WIP` | Currently being translated |
| `Translations Done` | Translations received and integrated and ready for QA |
| `QA WIP` | Currently being tested, bugs being reported |
| `QA Done` | Passed all tests, ready to go live |

### Resources labels

These are labels that generally do not change, unless the need for a specific resource (like design) has been identified along the way (though if an issue has clear requirements from the start, this happens rarely).

These help team members at a glance _if_ their input is needed and _where_.

**Example:** As a **Designer**, I will work on projects that have the label `Needs Design`. An issue without this label I would check only to see what my team mates are going to work on and keep up to date.

| Value | Description |
| -- | -- |
| `Needs Copy` | Requires input from **Copywriter** |
| `Needs Design` | Requires input from **Designer** |
| `Needs Frontend` | Requires input from **Developer** |
| `Needs Translations` | Requires input from **Translation Manager** |
| `Needs QA` | Requires input from **QA Analyst** |

These labels also help filtering the [GitLab project](https://gitlab.com/eyeo/websites/web.adblockplus.org/boards/575718) in order for team members to see only issues relevant to them. Pre-saved boards can be found when navigating to the GitLab project and [choosing one](/gitlab.com/img/switch-boards.png) you're interested in.

### Other labels

Find below a list of other labels that are in use and what they mean. 

| Value | Description |
| -- | -- |
| `Maintenance` | Small task, generally for a **Developer** that implies small changes with normal or low priority |
| `Draft` | Issue is currently being reviewed and is in the process of gathering & writing requirements |
| `Analyze` | Issue has been acknowledged and will be processed|
| `I'm stuck` | Indicates if something has been blocked for quite some time |
| `Impediment` | Issues that appeared as an emergency and are interrupting and blocking others |

**Important notes:**

1. Issues that are in the [`Open` column](#open) but do not have a label have not been touched yet. Add the label `Analyze` once the issue has been acknowledged by the team and some small progress made (for example, starting discussions, gathering feedback from people).
1. Add the label `Draft` to an issue when work has started to gather and write requirements. 
1. If an issue has the label `Impediment`, it trumps all other issues in terms of priority.

## Problems in the process

Below you can find some possible hints that there are problems in the change process. Learn what to look for in order to understand potential solutions.

### Too many issues in a column

At times, you might see columns with more than 3 issues. Depending on the type of column (WIP/Done), see which scenario applies:

#### If it's a _Done_ type column

Example of a _Done_ type column: `Creative Done`

This might indicate that there's a bottleneck in the next column to the right (e.g. If there's 3+ issues in `Creative Done`, this could mean a bottleneck from the **Product Manager**).

It can also indicate that the corresponding [labels](#labels) have not been updated, even though progress has been made. For example, a **Developer** has already started working on an issue but hasn't moved it from the `Specs/Requirements Done` column.

#### If it's a _WIP_ type column 

Example of a _WIP_ type column: `Creative WIP`

This should not be the case in general as we only have 1-2 team members per role (**Copywriter**, **Designer**, etc.) and in no situation is it plausible for a person to work on 3 or more topics at the same time. 

If an issue is not actively being worked on, it should be moved back to the previous _Done_ column.

**Example:** Issues A, B, C are in the `Dev WIP` column but C isn't being worked on actively. Though progress has been made, it isn't ready to be moved into `Dev Done`. It should be moved back to the `Specs/Requirements WIP` column.

The only exception to this is the `Translations WIP` column, where you could end up with 3-4 projects that are waiting for translations and review. However, it's always best to check.

More than 3+ issues in the _WIP_ column can also indicate that the [labels](#labels) haven't been updated and, as a result, the projects remained in limbo.

### Too much time in a column 

Every now and then, you might asses that an issue has been spending too much time in the same column.

This can indicate that the [labels](#labels) haven't been updated.

It can also suggest a possible bottleneck:

#### If it's a _Done_ type column

This might mean that there's a bottleneck in the next _WIP_ column to the right.

**Example:** An issue spent a lot of time in `Creative Done`. This could indicate a bottleneck with the **Product Manager** as it should have progressed to the `Specs/Requirements WIP` column.

It's always best to check the issue itself for more details and reach out to the team members that handle their work in the next _WIP_ column. This is because, on occasion, it can also mean that, for example, the team member that would normally drag the issue in the respective _WIP_ column is actually missing information. In this case, the issue should be moved to the previous _Done_ column.

**Example:** An issue spent a lot of time in the `Specs/Requirements Done`. It hasn't progressed to `Dev WIP` because a **Developer** is missing information. Either move the issue in `To Do` if a **Designer** and/or a **Copywriter** need to work on it more, or move it in the `Creative Done` column if the specifications or requirements need extra work. Always reach out to the appropriate people who need to re-work some details.

#### If it's a _WIP_ type column

This might mean that work has started on the issue, perhaps progress has been made but it stopped. As such, it should be moved to the previous _Done_ column.

**Example:** Work has began for an issue and as such, it was moved to `Dev WIP`. However, a **Developer** had to work on another issue that got its priority upgraded and progress stopped. The issue should be moved into the `Specs/Requirements Done` column and re-prioritized accordingly.

It can also mean someone is confusing the _WIP_ column with a personal "to do" list :D (we're all guilty, don't feel bad!). Try to keep only stuff you're working on in the _WIP_ type columns.

Sometimes, an issue can sit too long in a column if it depends on a contributor (e.g.: Legal or someone to perform code review). Check if that's the case and move ticket to the previous _Done_ column and add the [issue label](#labels) `I'm stuck`. If it's a regular occurrence, try finding alternative solutions.

