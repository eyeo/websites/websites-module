
# Translation process issue template snippet

Template of snippet to add to issue description for the translation process ([more details](https://docs.google.com/document/d/1teuNJ4vrYZlCckW7VdHLG6yZ-mqUt7v-fiWAszn-VGw/edit).

```
## Translation

### CSV File

Link to file

### Additional Information

Additional information if applicable

### Languages to translate into

Core languages and also XYZ

### Related tickets

Link to hub ticket, added by C&T team
Additional links as needed
```