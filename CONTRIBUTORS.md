# Contributors

## Owner

- Julian Doucette [@juliandoucette](https://gitlab.com/juliandoucette)

## Managers

- Luiza Ursachi [@lursachi](https://gitlab.com/lursachi)
- Winsley von Spee [@wspee](https://gitlab.com/wspee)

## Peers

- Manvel Saroyan [@saroyanm](https://gitlab.com/saroyanm)
- Ire Aderinokun [@i.aderinokun](https://gitlab.com/i.aderinokun)

## Guest contributors

- Lisa Bielik [@lisabielik](https://gitlab.com/lisabielik)
- Florian Damian [@f.damian](https://gitlab.com/f.damian)
- Yael Stein [@jaels](https://gitlab.com/jaels)

## Past contributors

- Wladimir Palant
- Sebastian Noack
- Felix Dahlke [@fhdahlke](https://gitlab.com/fhdahlke)
